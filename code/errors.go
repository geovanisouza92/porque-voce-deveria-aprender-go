package main

import (
	"errors"
	"fmt"
)

// START OMIT
func add(a, b int) (int, error) { // HL
	if a > 0 && b > 0 {
		return a + b, nil
	}
	return 0, errors.New("Só consigo somar números positivos.")
}

// END OMIT

// START 2 OMIT
func main() {
	res, err := add(2, -1)
	if err != nil { // HL
		fmt.Println(err)
		return
	}
	fmt.Println(res)
}

// END 2 OMIT
