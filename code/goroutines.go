package main

// START OMIT
func Announce(message string, delay time.Duration) {
	go func() {
		time.Sleep(delay)
		fmt.Println(message)
	}()
}

// END OMIT

func main() {
}
