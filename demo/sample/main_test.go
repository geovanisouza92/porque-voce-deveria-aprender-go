package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestQuestion(t *testing.T) {
	rec := httptest.NewRecorder()

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	questions(rec, req)

	contentType := rec.Header().Get("Content-Type")
	if contentType != "application/json; charset=utf-8" {
		t.Errorf("Content-Type incorreto: %s", contentType)
	}

	if rec.Code != 200 {
		t.Errorf("Status code incorreto: %d", rec.Code)
	}
}
